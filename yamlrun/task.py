import types
from typing import List
import importlib
import logging

from .exception import (
    YAMLFormatError,
    TaskError,
    ClassNotFoundError,
)


ClassList = List[type]


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
fh = logging.FileHandler('app.log')
fh.setLevel(logging.INFO)
logger.addHandler(fh)


def eval_script(script, ctx: dict):
    if script:
        lambda_script = 'lambda _: {}'.format(script)
        fn = eval(lambda_script, {}, ctx)
    return fn if script and isinstance(fn, types.FunctionType) else None


def run_wrapper(f):
    def _run(self, _in, ctx, *args, **kwargs):
        logger = logging.getLogger(__name__)
        nest = self._nest
        idx, indent = '', ''
        if nest:
            idx = ".".join([str(n) for n in nest])
            indent = " " * len(idx)
            name = self.spec.get('name')
            name = " # " + name if name else ''
            logger.info('-{}@ {} RUN [ {} ]{}'.format(indent, idx, self.spec.get('class'), name))

            before_ctx = ctx.copy()
        # Pre process
        pre_script = self.spec.get('pre_process')
        pre_fn = eval_script(pre_script, ctx)
        if pre_fn:
            logger.info('-{}@ pre_process: {}'.format(indent, pre_script))
            _in = pre_fn(_in)
        _out, ctx = f(self, _in, ctx, *args, **kwargs)
        # Post process
        post_script = self.spec.get('post_process')
        post_fn = eval_script(post_script, ctx)
        if post_fn:
            logger.info('-{}@ post_process: {}'.format(indent, post_script))
            _out = post_fn(_out)

        if nest:
            if _in != _out:
                s_in = _in[:40] if isinstance(_in, str) and len(_in) > 40 else _in
                s_out = _out[:40] if isinstance(_out, str) and len(_out) > 40 else _out
                logger.info(' {0}| {0} value changed: [{1}] => out: [{2}]'.format(indent, s_in, s_out))
            if before_ctx != ctx:
                logger.info(' {0}| {0} before: {1}'.format(indent, before_ctx))
                logger.info(' {0}| {0} after: {1}'.format(indent, ctx))
            logger.info('-{0}|_{1} END'.format(indent, idx))
        return _out, ctx
    return _run


class BaseTask:
    __required_keys = set()
    __spec_defaults = {
        'name': '',
    }

    def __init__(self, spec: dict, classes: ClassList):
        self._spec = spec.copy()
        self._required_keys = set()
        self._spec_defaults = {
            "class": self.__class__.__name__,
        }

        # from base class to superclass
        for c in reversed(self.__class__.mro()[:-1]):
            # overwrite with subclass attributes
            self._required_keys.update(self._clsattr(c, 'required_keys', set()))
            self._spec_defaults.update(self._clsattr(c, 'spec_defaults', {}))

        self._nest = []

    def _clsattr(self, cls: type, attr_name: str, _defval):
        attr = getattr(self, '_{}__{}'.format(cls.__name__, attr_name), _defval)
        return attr

    @property
    def required_keys(self):
        """Get the required keys for this class"""
        return self._required_keys

    @property
    def spec(self):
        """Get the spec for this class"""
        spec = self._spec_defaults.copy()
        spec.update(self._spec)
        return spec

    def spec_with(self, context: dict):
        spec = self.spec
        for k, v in spec.items():
            if isinstance(v, str):
                spec[k] = v.format(**context)
        return spec

    @run_wrapper
    def run(self, _in, context: dict):
        _out, change = self.main(_in, self.spec_with(context))
        context.update(change)
        return _out, context

    def main(self, _in, run_spec: dict):
        raise NotImplementedError()


class PassTask(BaseTask):
    def main(self, _in, run_spec: dict):
        return (_in, {})


def import_class(name):
    """
    Import classes
    """

    p, c = name.rsplit('.', 1)
    mod = importlib.import_module(p)
    return getattr(mod, c)


class ImportTask(BaseTask):

    __required_keys = {
        'imports'
    }

    def __init__(self, spec: dict, classes: ClassList):
        super().__init__(spec, classes)
        if 'imports' in spec:
            imps = spec.get('imports')
            if isinstance(imps, list):
                for name in imps:
                    cls = import_class(name)
                    classes.append(cls)
            else:
                raise YAMLFormatError('The imports must be a list. (%s)' % type(imps))

    def main(self, _in, run_spec: dict):
        return _in, {}


class EnvTask(BaseTask):

    __spec_defaults = {
        'envs': []
    }

    def main(self, _in, run_spec: dict):
        change = {}
        import os
        os.environ
        for name in run_spec.get('envs'):
            change[name] = os.environ.get(name)
        return (_in, change)


class PrintTask(BaseTask):
    def main(self, _in, run_spec: dict):
        print(_in)
        return (_in, {})


class EvalTask(BaseTask):
    __required_keys = {
        'script'
    }

    @run_wrapper
    def run(self, _in, context: dict):
        spec = self.spec_with(context)
        script = spec.get('script')
        _locals = {
            '_ctx': context.copy(),
            '_spec': spec.copy(),
            '_': _in,
        }
        _out = eval(script, {}, _locals) if script else _in
        return _out, context


class Container(BaseTask):
    __required_keys = {
        'sub_tasks'
    }

    __spec_defaults = {
        'sub_tasks': [],
    }

    def __init__(self, spec: dict, classes: ClassList):
        super().__init__(spec, classes)
        tasks = []
        for sc in self.spec.get('sub_tasks'):
            tasks.append(inject_task(sc, classes))
        self._tasks = tasks

    @run_wrapper
    def run(self, _in, context: dict):
        _out = _in
        for i, t in enumerate(self._tasks):
            nest = self._nest.copy()
            nest.extend([i+1])
            t._nest = nest
            _out, context = t.run(_in, context)
            _in = _out
        return _out, context

    def main(self, _in, run_spec: dict):
        return _in, {}


class ForEachContainer(Container):
    @run_wrapper
    def run(self, _in_list, context: dict):
        if not hasattr(_in_list, '__iter__'):
            return _in_list, context

        _out_list = []
        for ii, _in in enumerate(_in_list):
            _ctx = context.copy()
            for ti, t in enumerate(self._tasks):
                nest = self._nest.copy()
                nest.extend([ii+1, ti+1])
                t._nest = nest
                _out, _ctx = t.run(_in, _ctx)
                _in = _out
            _out_list.append(_out)
        return _out_list, context

    def main(self, _in, run_spec: dict):
        return _in, {}


def inject_task(spec, classes):
    if type(spec) != dict:
        raise TaskError
    cls_name = spec.get('class', 'PassTask')
    for cls in classes:
        if cls_name == cls.__name__:
            break
    else:
        raise ClassNotFoundError('"%s" is not found in enable classes.' % cls_name)
    task = cls(spec, classes)
    return task
