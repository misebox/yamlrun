import yaml
import yamlrun.task

from .task import Container
from .exception import (
    YAMLParseError,
    YAMLFormatError,
)


def task_classes():
    """
    Add Task classes
    """
    classes = []
    for cname in dir(yamlrun.task):
        cls = getattr(yamlrun.task, cname)
        if isinstance(cls, type) and issubclass(cls, yamlrun.task.BaseTask):
            classes.append(cls)
    return classes


def load_yaml(yml_str):
    """
    Parse YAML
    """
    try:
        top_obj = yaml.load(yml_str)
    except yaml.YAMLError as e:
        msg = e.problem
        raise YAMLParseError('YAML is invalid. ( %s )' % msg)

    # create plans
    classes = task_classes()
    if isinstance(top_obj, list):
        spec = {
            "sub_tasks": top_obj,
        }
        container = Container(spec, classes)
    else:
        k = type(top_obj).__name__
        raise YAMLFormatError('Top level object must be list, but "{}".'.format(k))

    return container
