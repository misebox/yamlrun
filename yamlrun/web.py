import requests
from http.cookiejar import CookieJar

from .task import BaseTask, run_wrapper


class HTTPRequestTask(BaseTask):
    __required_keys = {
        'url',
    }

    __spec_defaults = {
        'method': 'GET',
        'params': {},
        'data': {},
        'headers': {},
    }

    @run_wrapper
    def run(self, _in, context: dict):
        spec = self.spec
        session = context.get('session', requests.Session())
        if hasattr(session, 'cookies'):
            session.cookies = CookieJar()
        headers = context.get('headers', {})
        headers.update(spec.get('headers'))
        params = spec.get('params')

        url = spec.get('url')
        method = spec.get('method').upper()

        kwargs = {}
        if params:
            kwargs['params'] = params
        if headers:
            kwargs['headers'] = headers

        response = session.request(method, url, **kwargs)
        _out = response.text

        context['session'] = session
        context['headers'] = session.headers
        return _out, context
