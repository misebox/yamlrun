import re
from lxml import (
    html,
    )
from .task import (
    BaseTask,
    )


class RegexTask(BaseTask):
    __required_keys = {
        'pattern',
    }

    def main(self, _in, run_spec: dict):
        pattern = self.spec.get('pattern')
        _out = re.findall(pattern, _in, re.MULTILINE)
        change = {}
        return _out, change


class HTMLSelectTask(BaseTask):
    __required_keys = {
        'selector',
    }

    def main(self, _in, run_spec: dict):
        sel = self.spec.get('selector')
        e = html.fromstring(_in)
        _out = e.cssselect(sel)
        change = {}
        return _out, change
