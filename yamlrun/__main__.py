import sys
import os


args = sys.argv[1:]
if len(args) > 0:
    pardir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append(pardir)

    import yamlrun

    yamlfile = os.path.abspath(args.pop(0))
    yaml = ''
    with open(yamlfile) as f:
        yaml = f.read()

    container = yamlrun.load_yaml(yaml)
    ctx = {}
    _in = args.pop(0) if args else ''

    (_out, ctx) = container.run(_in, ctx)
    print(_out)

else:
    print('YAML file is required.')
