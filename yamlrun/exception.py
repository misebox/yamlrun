class YAMLParseError(Exception):
    pass


class YAMLFormatError(Exception):
    pass


class TaskError(Exception):
    pass


class ClassNotFoundError(TaskError):
    pass
