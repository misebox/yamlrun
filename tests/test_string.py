import unittest
from unittest.mock import patch, MagicMock, call
from yamlrun.string import RegexTask, HTMLSelectTask


class TestRegexTask(unittest.TestCase):
    def test_regex(self):
        spec = {
            'pattern': '^aa(.*)cc(.*)ee$',
        }
        _in = 'aaFIRSTccSECONDee\naaTHREEccFOURee'
        task = RegexTask(spec, [])
        _out, ctx = task.run(_in, {})
        self.assertEqual(_out, [('FIRST', 'SECOND'), ('THREE', 'FOUR')] )

    def test_regex_(self):
        spec = {
            'pattern': '^aa(.*)cc$',
        }
        _in = 'aaFIRSTcc\naaSECONDcc'
        task = RegexTask(spec, [])
        _out, ctx = task.run(_in, {})
        self.assertEqual(_out, ['FIRST', 'SECOND'])


class TestHTMLSelelctorTask(unittest.TestCase):
    def test_html(self):
        spec = {
            'selector': 'div p',
            'post_process': '[e.text for e in _]',
        }
        _in = '<div><p>hogehoge<p>fugofugo</div>'
        task = HTMLSelectTask(spec, [])
        _out, ctx = task.run(_in, {})
        self.assertEqual(_out, ['hogehoge', 'fugofugo'])
