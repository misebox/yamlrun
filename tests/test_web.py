import unittest
from unittest.mock import patch, MagicMock, call
from yamlrun.web import HTTPRequestTask


class TestHTTPRequestTask(unittest.TestCase):

    def test_default(self):
        spec = {
            'url': 'http://localhost/'
        }
        clases = []
        with patch('yamlrun.web.requests') as m:
            task = HTTPRequestTask(spec, [])
            task.run({}, {})
            self.assertEqual(m.mock_calls[0], call.Session())
            self.assertEqual(m.mock_calls[1],
                call.Session().request('GET', 'http://localhost/'))


    def test_get(self):
        spec = {
            'method': 'GET',
            'url': 'http://localhost/',
        }
        clases = []
        with patch('yamlrun.web.requests') as m:
            task = HTTPRequestTask(spec, [])
            task.run({}, {})
            self.assertEqual(m.mock_calls[0], call.Session())
            self.assertEqual(m.mock_calls[1],
                call.Session().request('GET', 'http://localhost/'))


    def test_post(self):
        spec = {
            'method': 'post',
            'url': 'http://localhost/',
        }
        clases = []
        with patch('yamlrun.web.requests') as m:
            task = HTTPRequestTask(spec, [])
            task.run({}, {})
            self.assertEqual(m.mock_calls[0], call.Session())
            self.assertEqual(m.mock_calls[1],
                call.Session().request('POST', 'http://localhost/'))


    def test_put(self):
        spec = {
            'method': 'put',
            'url': 'http://localhost/',
        }
        clases = []
        with patch('yamlrun.web.requests') as m:
            task = HTTPRequestTask(spec, [])
            task.run({}, {})
            self.assertEqual(m.mock_calls[0], call.Session())
            self.assertEqual(m.mock_calls[1],
                call.Session().request('PUT', 'http://localhost/'))
