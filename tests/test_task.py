import unittest
from unittest.mock import patch, MagicMock
from yamlrun import (
    load_yaml,
)
from yamlrun.web import HTTPRequestTask
from yamlrun.task import (
    import_class,
    inject_task,
    ClassNotFoundError,
    PassTask,
    EnvTask,
    EvalTask,
    ImportTask,
    Container,
    ForEachContainer,
)


class TestPassTask(unittest.TestCase):

    def test_noname_task(self):
        task = PassTask({}, PassTask)
        self.assertEqual(task.spec.get('name'), '')

    def test_named_task(self):
        test_name = 'hogehoge'
        spec = dict(name=test_name)
        task = PassTask(spec, [PassTask])
        self.assertEqual(task._spec.get('name'), test_name)

    def test_run_nothing_changed(self):
        task = PassTask({}, [])
        _in = 'input data'
        ctx = {}
        _out, out_ctx = task.run(_in, ctx.copy())
        self.assertEqual(_in, _out)
        self.assertEqual(ctx, out_ctx)


class TestTaskCreator(unittest.TestCase):
    def test_create_task(self):
        spec = {}
        classes = [PassTask]
        task = inject_task(spec, classes)
        self.assertIsInstance(task, PassTask)

    def test_create_task_env(self):
        spec = {
            'class': 'EnvTask',
            'envs': {},
        }
        classes = [PassTask, EnvTask]
        task = inject_task(spec, classes)
        self.assertIsInstance(task, EnvTask)

    def test_create_task_class_not_found(self):
        spec = {
            "class": "Dummy",
        }
        classes = [PassTask, EnvTask]
        with self.assertRaises(ClassNotFoundError):
            inject_task(spec, classes)


class TestEvalTask(unittest.TestCase):
    def test_with_pre_post_process(self):
        spec = {
            "pre_process": "_ + 2",
            "script": "_ * 2",
            "post_process": "_ ** 2",
        }
        task = EvalTask(spec, [])
        _out, ctx = task.run(1, {})
        self.assertEqual(_out, 36)


class TestImportTask(unittest.TestCase):
    def test_import_class(self):
        spec = {
            'imports': [
                'yamlrun.task.PassTask',
                'yamlrun.web.HTTPRequestTask',
            ],
        }
        classes = []
        task = ImportTask(spec, classes)
        self.assertEqual(classes, [PassTask, HTTPRequestTask])


class TestContainer(unittest.TestCase):
    def test_run(self):
        spec = {
            'sub_tasks': [
                {
                    'class': 'EvalTask',
                    'script': '_ + 2',
                },
                {
                    'class': 'EvalTask',
                    'script': '_ * 2',
                },
            ]
        }
        classes = [EvalTask]
        task = Container(spec, classes)
        _out, ctx = task.run(1, {})
        self.assertEqual(_out, 6)


class TestForEachContainer(unittest.TestCase):
    def test_run(self):
        spec = {
            'sub_tasks': [
                {
                    'class': 'EvalTask',
                    'script': '_ + 2',
                },
                {
                    'class': 'EvalTask',
                    'script': '_ * 2',
                },
            ]
        }
        classes = [EvalTask]
        task = ForEachContainer(spec, classes)
        _out, ctx = task.run([1, 2, 3], {})
        self.assertEqual(_out, [6, 8, 10])

if __name__ == '__main__':
    unittest.main()
