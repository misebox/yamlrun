import unittest
from unittest.mock import call, patch, MagicMock, PropertyMock

from yamlrun import (
    load_yaml,
)

from yamlrun.exception import (
    YAMLParseError,
    YAMLFormatError,
    ClassNotFoundError,
)

from yamlrun.web import (
    HTTPRequestTask
)

from yamlrun.task import (
    PassTask,
    ImportTask,
)


class TestLoadYaml(unittest.TestCase):

    def test_3_tasks(self):
        yml_str = """
        - name: first
        - name: second
        - name: third
        """
        f = load_yaml(yml_str)
        self.assertEqual(len(f._tasks), 3)
        self.assertEqual(f._tasks[0].spec.get('name'), 'first')
        self.assertEqual(f._tasks[1].spec.get('name'), 'second')
        self.assertEqual(f._tasks[2].spec.get('name'), 'third')

    def test_various_tasks(self):
        yml_str = """
        - name: task_1

        - name: task_2
          class: PassTask

        - name: task_3
          class: PassTask

        - name: task_4
        """
        f = load_yaml(yml_str)
        self.assertEqual(len(f._tasks), 4)
        self.assertEqual(f._tasks[0].spec.get('name'), 'task_1')
        self.assertIsInstance(f._tasks[0], PassTask)
        self.assertEqual(f._tasks[1].spec.get('name'), 'task_2')
        self.assertIsInstance(f._tasks[1], PassTask)
        self.assertEqual(f._tasks[2].spec.get('name'), 'task_3')
        self.assertIsInstance(f._tasks[2], PassTask)
        self.assertEqual(f._tasks[3].spec.get('name'), 'task_4')
        self.assertIsInstance(f._tasks[3], PassTask)

    def test_error_invalid_yaml(self):
        yml_str = """
        - invalid
        YAML
        """
        with self.assertRaises(YAMLParseError):
            load_yaml(yml_str)

    def test_error_the_key_named_tasks_not_found(self):
        yml_str = """
        env_vars:
          var1: 10
          var2: 20
        """
        with self.assertRaises(YAMLFormatError):
            load_yaml(yml_str)


    def test_error_tasks_is_not_a_list(self):
        yml_str = """
        tasks:
          name: NotListButDict
          class: dict
        """
        with self.assertRaises(YAMLFormatError):
            load_yaml(yml_str)


if __name__ == '__main__':
    unittest.main()
