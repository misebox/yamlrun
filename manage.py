#!/usr/bin/env python
# coding: utf-8

import click
import subprocess


@click.group()
def cmd():
    pass


@cmd.command()
@click.argument('targets', nargs=-1)
def lint(targets):

    if len(targets) > 0:
        for t in targets:
            click.echo('Lint {}.'.format(t))
            subprocess.call('flake8 yamlrun/{}.py'.format(t), shell=True)
    else:
        click.echo('Lint all !')
        subprocess.call('flake8 yamlrun/', shell=True)


@cmd.command()
@click.argument('targets', nargs=-1)
def cov(targets):

    click.echo('Coverage !')
    subprocess.call('nosetests', shell=True)
    subprocess.call('python -m http.server 9001', shell=True)


@cmd.command()
@click.argument('targets', nargs=-1)
def test(targets):

    click.echo('Test all !')
    subprocess.call('flake8 yamlrun/', shell=True)
    subprocess.call('nosetests', shell=True)


@cmd.command()
def dev():
    click.echo('dev !')


@cmd.command()
@click.argument('services', nargs=-1)
def dc(services):
    click.echo('Docker !')
    subprocess.call('docker-compose up {}'.format(" ".join(services)), shell=True)


@cmd.command()
def clean():
    click.echo('Clean !')
    subprocess.call('for f in `find yamlrun tests -name "__pycache__"`; do ls -d "$f"; rm -r "$f"; done', shell=True)
    targets = ["htmlcov", ".coverage"]
    for t in targets:
        subprocess.call('[ -e "{0}" ] && ls -d "{0}" && rm -r "{0}"'.format(t), shell=True)


def main():
    cmd()


if __name__ == '__main__':
    main()
